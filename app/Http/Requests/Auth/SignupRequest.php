<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idRol' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'nombres' => 'required',
            'apellidos' => 'required',
            'dni' => ['nullable','max:8'],
            'telefono' => 'nullable',
            'correo' => 'nullable',
        ];
    }
}
