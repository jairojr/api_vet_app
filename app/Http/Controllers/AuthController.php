<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\SignupRequest;
use App\Models\Cliente;
use App\Models\Persona;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     *  Crear una cuenta
     *
    */
    public function signup(SignupRequest $request) {

        // Registro en la tabla users
        $user = new User();
        $user->idRol = $request->idRol;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->save();

        // Registro en la tabla personas
        $persona = new Persona();
        $persona->nombres = $request->nombres;
        $persona->apellidos = $request->apellidos;
        $persona->dni = $request->dni;
        $persona->telefono = $request->telefono;
        $persona->correo = $request->correo;
        $persona->save();

        /*
         * Registro de la relacion
         *
         * users <- clientes -> personas
         * */
        $cliente = new Cliente();
        $cliente->idUsuario = $user->id;
        $cliente->idPersona = $persona->id;
        $cliente->save();

        return response()->json([
            'res' => true,
            'msg' => 'Usuario registrado correctamente',
            'users' => $user,
            'personas' => $persona,
            'cliente' => $cliente,
        ], 200);

    }

    /**
     *  Iniciar sesion
     *
     */
    public function login(LoginRequest $request) {
        $user = User::where('username', $request->username)->first();

        if (!$user || !Hash::check($request->password, $user->password)){
            throw ValidationException::withMessages([
                'message' => ['Invalid login or password.'],
            ]);
        }

        $token = $user->createToken($request->username)->plainTextToken;
        return response()->json([
            'token' => $token
        ],200);
    }

    /**
     *  Cerrar sesion
     *
     */
    public function logout(Request $request) {
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'message' => 'token delete successful',
            'data' => $request->user()
        ]);
    }
}
