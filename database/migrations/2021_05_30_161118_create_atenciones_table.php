<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtencionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atenciones', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idMascota')->unique()->constrained('mascotas');
            $table->foreignId('idControl')->unique()->constrained('controles');
            $table->foreignId('idClinica')->unique()->nullable()->constrained('clinicas');
            $table->foreignId('idEstetica')->unique()->nullable()->constrained('esteticas');
            $table->date('fechaAtencion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atenciones');
    }
}
